#!/bin/bash


# nasus pinning
# numactl -i 1 ./bin/hashmap.none.dcotl      -prefill-insert -nprefill 1  -nwork 128 -i 25 -d 25 -k 1000000 -t 20000 -pin 64-127,192-255,0-63,128-191; echo "Above Data is for Hashmap DCOTL"; echo ""
numactl -i 1 ./bin/hashmap.none.mvcc_dcotl -prefill-insert -nprefill 1 -nwork 128 -i 25 -d 25 -k 1000000 -t 20000 -pin 64-127,192-255,0-63,128-191; echo "Above Data is for Hashmap MVCC_DCOTL"; echo ""


## prefilling disabled
# numactl -i 1 ./bin/hashmap.none.dcotl 	 -nprefill 128 -nwork 128 -i 25 -d 25 -k 1000000 -t 20000 -pin 64-127,192-255,0-63,128-191; echo "Above Data is for Hashmap DCOTL"; echo ""
# numactl -i 1 ./bin/hashmap.none.mvcc_dcotl -nprefill 128 -nwork 128 -i 25 -d 25 -k 1000000 -t 20000 -pin 64-127,192-255,0-63,128-191; echo "Above Data is for Hashmap MVCC_DCOTL"; echo ""

# numactl -i 1 ./bin/btree.none.dcotl      -prefill-insert -nwork 128 -i 10 -d 10 -k 1000000 -t 20000 -pin 64-127,192-255,0-63,128-191; echo "Above Data is for Btree DCOTL"
# numactl -i 1 ./bin/btree.none.mvcc_dcotl -prefill-insert -nwork 128 -i 10 -d 10 -k 1000000 -t 20000 -pin 64-127,192-255,0-63,128-191; echo "Above Data is for Btree MVCC_DCOTL"

# numactl -i 1 ./bin/abtree.none.dcotl -prefill-insert -nwork 128 -i 25 -d 25 -k 1000000 -t 20000 -pin 64-127,192-255,0-63,128-191
# numactl -i 1 ./bin/redblack.none.dcotl -prefill-insert -nwork 128 -i 25 -d 25 -k 1000000 -t 20000 -pin 64-127,192-255,0-63,128-191


# jax pinning
# numactl --interleave=all ./bin/hashmap.none.dcotl -nwork 128 -nprefill 128 -i 0 -d 0 -k 1000000 -t 20000 -pin 0-23,96-119,24-47,120-143,48-71,144-167,72-95,168-191