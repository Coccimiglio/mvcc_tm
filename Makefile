FLAGS =
LDFLAGS =
GPP = g++-10

### if libpapi is installed but you do not want to use it, invoke make with extra argument "has_libpapi=0"
# has_libpapi=$(shell setbench/microbench/_check_lib.sh papi)
has_libpapi=1
ifneq ($(has_libpapi), 0)
	FLAGS += -DUSE_PAPI
	LDFLAGS += -lpapi
endif

### if libnuma is installed but you do not want to use it, invoke make with extra argument "has_libnuma=0"
has_libnuma=$(shell setbench/microbench/_check_lib.sh numa)
ifneq ($(has_libnuma), 0)
	FLAGS += -DUSE_LIBNUMA
	LDFLAGS += -lnuma
endif

use_asan=0
ifneq ($(use_asan), 0)
	LDFLAGS += -fsanitize=address -static-libasan
endif

use_asserts=0
ifeq ($(use_asserts), 0)
	FLAGS += -DNDEBUG
endif

use_fopenmp=0
ifeq ($(use_fopenmp), 1)
	FLAGS += -fopenmp
endif

use_timelines=0
ifeq ($(use_timelines), 1)
	FLAGS += -DMEASURE_TIMELINE_STATS
endif

hang_for_debug=0
ifeq ($(hang_for_debug), 1)
	FLAGS += -DHANG_ON_TIME_UP_FAILURE
endif


no_optimize=0
ifeq ($(no_optimize), 1)	
	FLAGS += -O0
	FLAGS += -fno-inline-functions -fno-inline-functions-called-once -fno-optimize-sibling-calls
	FLAGS += -fno-default-inline -fno-inline
	FLAGS += -fno-omit-frame-pointer
	FLAGS += -DHANG_ON_TIME_UP_FAILURE
	FLAGS += -DNO_OPTIMIZE_IS_ON
else
	FLAGS += -O2
	FLAGS += -Wno-inline
	FLAGS += -fomit-frame-pointer	
	# FLAGS += -finline-limit=1000000
endif
FLAGS += -mcx16 #needed for DWCAS (cmpxchg16b)
FLAGS += -DMAX_THREADS_POW2=256
FLAGS += -DCPU_FREQ_GHZ=2.1 #$(shell ./experiments/get_cpu_ghz.sh)
FLAGS += -DMEMORY_STATS=if\(0\) -DMEMORY_STATS2=if\(0\)

CFLAGS = -std=c++17
CFLAGS += -g

FLAGS += -DNO_CLEANUP_AFTER_WORKLOAD ### avoid executing data structure destructors, to save teardown time at the end of each trial (useful with massive trees)
FLAGS += -DGSTATS_MAX_THREAD_BUF_SIZE=2097152
FLAGS += $(xargs)

LDFLAGS += -Lsetbench/lib
LDFLAGS += -I. -Isetbench/microbench `find setbench/common -type d | sed s/^/-I/`
LDFLAGS += -lpthread
LDFLAGS += -latomic
LDFLAGS += -ldl
LDFLAGS += -mrtm

.PHONY: all
all:

bin_dir=bin
dir_guard:
	@mkdir -p $(bin_dir)

clean:
	rm $(bin_dir)/*.out

# FLAGS += $(bin_dir)/death_handler.o -Itmlib/common
# death_handler: dir_guard
# 	$(GPP) tmlib/common/death_handler.cc -c -o $(bin_dir)/death_handler.o -O3 -g -ldl -Wno-unused-result


################################################################################
#### DECIDING WHICH COMBINATIONS OF: {DATA STRUCTURE, RECLAIMER, TM} TO USE
################################################################################

$(info ------------------------------------)

# RECLAIMERS=debra none
RECLAIMERS=none
DS_DIR=ds_dcotl

TM_name=dcotl
partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    ALL_DS=$(patsubst ds_$(TM_name)/%/adapter.h,%,$(wildcard ds_$(TM_name)/*/adapter.h))
else
	ALL_DS=$(foreach partial_name,$(partial_algorithm_names), $(patsubst ds_$(TM_name)/%/adapter.h,%,$(wildcard ds_$(TM_name)/$(partial_name)*/adapter.h)))
endif
define create-target-new-ds-reclaim-$(TM_name) =
_$(1).$(2).$(TM_name): dir_guard	
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).$(TM_name) -D$(TM_name)  -DUSE_GSTATS_USER_HANDLER_H_FILE -I./ -Ids_$(TM_name)/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(CFLAGS) $(LDFLAGS)	
new-ds-reclaim-$(TM_name): _$(1).$(2).$(TM_name) 
endef
$(foreach ds,$(ALL_DS), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-new-ds-reclaim-$(TM_name),$(ds),$(reclaim),$(TM_name))) \
	) \
)
# all: new-ds-reclaim-$(TM_name)


TM_name=mvcc_dcotl
partial_algorithm_names=
ifeq ($(partial_algorithm_names),)
    ALL_DS=$(patsubst ds_$(TM_name)/%/adapter.h,%,$(wildcard ds_$(TM_name)/*/adapter.h))
else
	ALL_DS=$(foreach partial_name,$(partial_algorithm_names), $(patsubst ds_$(TM_name)/%/adapter.h,%,$(wildcard ds_$(TM_name)/$(partial_name)*/adapter.h)))
endif
define create-target-new-ds-reclaim-$(TM_name) =
_$(1).$(2).$(TM_name): dir_guard	
	$(GPP) setbench/microbench/main.cpp -o $(bin_dir)/$(1).$(2).$(TM_name) -D$(TM_name)  -DUSE_GSTATS_USER_HANDLER_H_FILE -I./ -Ids_$(TM_name)/$(1) -DDS_TYPENAME=$(1) -DRECLAIM_TYPE=$(2) $(FLAGS) $(CFLAGS) $(LDFLAGS)	
new-ds-reclaim-$(TM_name): _$(1).$(2).$(TM_name) 
endef
$(foreach ds,$(ALL_DS), \
	$(foreach reclaim,$(RECLAIMERS), \
		$(eval $(call create-target-new-ds-reclaim-$(TM_name),$(ds),$(reclaim),$(TM_name))) \
	) \
)
# all: new-ds-reclaim-$(TM_name)


