#pragma once

#ifndef VOLATILE_MEMORY_REGION_SIZE
	#define VOLATILE_MEMORY_REGION_SIZE 128*1024*1024*1024ULL //GB
#endif

#ifdef USE_MVCC_TM   
  	#define TM_NAME "MVCC_DCTL"
	#include "../mvcc_dctl_snapshot_writers_inf_vlist.hpp"
  
	#define TM_NS ns_mvcc_tm
	#define TM_T TM_NS::MVCC_DCOTL
	#define TM_TYPE_T TM_NS::tx_field
#elif defined(USE_DCTL)
	#define TM_NAME "DCTL"
	#include "../DCOTL.hpp"
	#define TM_NS ns_dcotl
	#define TM_T TM_NS::DCOTL
	#define TM_TYPE_T TM_NS::tx_field
#else
	#define TM_NAME "Default TM: DCTL"
	#include "../DCOTL.hpp"
	#define TM_NS ns_dcotl
	#define TM_T TM_NS::DCOTL
	#define TM_TYPE_T TM_NS::tx_field
#endif

