#include <atomic>
#include "DCOTL.hpp"

namespace ns_dcotl {
    // Global/singleton to hold all the thread registry functionality
    ThreadRegistry gThreadRegistry {};
    // Array of locks
    std::atomic<uint64_t> *gHashLock {nullptr};
    // Global clock for TL2
    alignas(128) std::atomic<uint64_t> gClockPaddingA {0};
    alignas(128) std::atomic<uint64_t> gClock {1};
    alignas(128) std::atomic<uint64_t> gClockPaddingB {0};
    // TM singleton
    DCOTL gDCOTL {};
    // Thread-local data of the current ongoing transaction
    thread_local OpData* tl_opdata {nullptr};
    // This is where every thread stores the tid it has been assigned when it calls getTID() for the first time.
    // When the thread dies, the destructor of ThreadCheckInCheckOut will be called and de-register the thread.
    thread_local ThreadCheckInCheckOut tl_tcico {};
    // Helper function for thread de-registration
    void thread_registry_deregister_thread(const int tid) {
        gThreadRegistry.deregister_thread(tid);
    }
    // This is called from persist::load()/store() and endTx() if the read-set validation fails.
    [[noreturn]] void abortTx(OpData* myd) {
        myd->writeSet.rollback(myd->tid);
        uint64_t nextClock = gClock.fetch_add(1);
        // printf("aborted, nextclock = %ld\n", nextClock);
        // Unlock with the new sequence
        myd->writeSet.unlock(nextClock, myd->tid);
        myd->numAborts++;
        std::longjmp(myd->env, 1);
    }
}