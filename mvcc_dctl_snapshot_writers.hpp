/*
 * 
 * Details:
 *  Lock table entries have lock + bloom filter indicating if the addr is versioned
 *      there is a corresponding table of version lists where 
 *      an entry is an initially small number of contiguous version lists
 *      expandable (but obviously not contiguously)
 *  Fixed size version lists
 *  Version lists are NOT expandable 
 *      - this requires nodes to have next ptrs which we will need to think about how the cache line usage works with this
 *  
 *  
 * This is a prototype of MVCC_DCOTL based on the TrinityVRTL2 implementation. The original
 *   authors of Trinity are Pedro Ramalhete: pramalhe@gmail.com, Andreia Correia: andreia.veiga@unine.ch
 *   and Pascal Felber: pascal.felber@unine.ch
 *
 *   Most of this file is from https://github.com/pramalhe/durabletx/blob/master/ptms/trinity/TrinityVRTL2.hpp
 */
#pragma once

#include <atomic>
#include <cassert>
#include <functional>
#include <cstring>
#include <thread>       // Needed by this_thread::yield()
#include <sys/mman.h>   // Needed if we use mmap()
#include <sys/types.h>  // Needed by open() and close()
#include <sys/stat.h>
#include <linux/mman.h> // Needed by MAP_SHARED_VALIDATE
#include <fcntl.h>
#include <unistd.h>     // Needed by close()
#include <filesystem>   // Needed by std::filesystem::space()
#include <type_traits>
#include <sched.h>      // sched_setaffinity()
#include <csetjmp>      // Needed by sigjmp_buf
#include <cstdlib>      // Needed by exit()

#include "stats.h"

#include "tm_alloc2.h"
#include "bloom_filter.h"

namespace ns_mvcc_tm {

#define ENABLE_WRITE_SNAPSHOT 1
// #define DISABLE_TM_FREE 1

// #define DISABLE_MVCC 1
#define TRACK_SNAPSHOT_METRICS 1
#define TRACK_REGULAR_METRICS 1

#define ENABLE_VTN_ZEROING 1
#ifdef ENABLE_VTN_ZEROING 
    #define IN_PROGRESS_UNVERSIONING_ADDR   ((uint64_t*)0xffffffffffffULL)
    #define UNVERSIONED_ADDR                nullptr
#endif

static const uint64_t RETRIES_BEFORE_SNAPSHOT = 40;

static const uint64_t VERSION_LIST_SIZE = 6;

static const uint64_t UNVERSION_RANDOM_ROLL_THRESHOLD_WRITER = 0;
static const uint64_t UNVERSION_RANDOM_ROLL_THRESHOLD_READER = 0;

static const uint64_t PER_THREAD_VLIST_POOL_INIT_CAPACITY = 128;


//GUY::TODO:: we dont really need to use their tm allocator but for now I left it in
//  this requires knowing the max memory size at compile time which is restrictive
//  consider adding the ability to have multiple mapped regions so we dont cap memory at compile time
#ifndef VOLATILE_MEMORY_REGION_SIZE
    #define VOLATILE_MEMORY_REGION_SIZE 1*1024*1024*1024ULL //GB
#endif

// Maximum number of registered threads that can execute transactions
static const int REGISTRY_MAX_THREADS = 256;

// TL2 constants
static const uint64_t LOCKED = 0x8000000000000000ULL;
static const int TX_IS_NONE         = 0;
static const int TX_IS_READ         = 2;
static const int TX_IS_UPDATE       = 4;
static const int TX_IS_SNAPSHOT     = 1; //snapshots & 1 == true

static const uint64_t NUM_LOCKS = 4*1024*1024;
// static const uint64_t NUM_LOCKS_PRIME = 4194301;
// static const uint64_t LOCK_DIVISOR = 257;

// Function that hashes a address to a lock index.
inline __attribute__((always_inline)) static uint64_t hidx(size_t addr) {
    return ((addr/256) & (NUM_LOCKS-1));
    // return ((addr/LOCK_DIVISOR) % NUM_LOCKS_PRIME);
    // return ((addr/256) % NUM_LOCKS);
}

static const int TID_SHIFT = 53;
static const int LOCK_SHIFT = 63;

static const uint64_t LOCK_MASK = 0x8000000000000000ULL;
static const uint64_t TID_MASK = 0x7fe0000000000000ULL;
static const uint64_t TS_MASK = 0x001fffffffffffffULL;

// GUY::TODO can probably just convert these helper functions to macros 
//  assuming compiler actually inlines this should be fine
// Helper functions
inline __attribute__((always_inline)) static bool isLocked(uint64_t sl) { 
    return (sl & LOCKED); 
}

inline __attribute__((always_inline)) static bool isUnlocked(uint64_t sl) { 
    return (!(sl & LOCKED)); 
}

inline __attribute__((always_inline)) static uint64_t getTid(uint64_t sl) {
    return ((sl & TID_MASK) >> TID_SHIFT);
}

inline __attribute__((always_inline)) static uint64_t getTS(uint64_t sl) {
    return (sl & TS_MASK);
}

inline __attribute__((always_inline)) static bool isReadConsistent(uint64_t rClock, uint64_t tid, uint64_t sl) {
    return ((isUnlocked(sl) && getTS(sl) < rClock) || (getTid(sl) == tid));
}

inline __attribute__((always_inline)) static bool isSnapshotReadConsistent(uint64_t rClock, uint64_t tid, uint64_t sl) {
    return ((isUnlocked(sl) && getTS(sl) < rClock) || (getTid(sl) == tid && getTS(sl) < rClock));
}

// inline __attribute__((always_inline)) static bool isWriteConsistent(uint64_t tid, uint64_t sl) {    
//     return (isUnlocked(sl) || (isLocked(sl) && getTid(sl) == tid));
// }

inline __attribute__((always_inline)) static bool isWriteConsistent(uint64_t rClock, uint64_t tid, uint64_t sl) {
    return isReadConsistent(rClock, tid, sl);
}

inline __attribute__((always_inline)) static bool tryWriteLock(uint64_t tid, std::atomic<uint64_t>* mutex, uint64_t sl) {
    //in the context of its usage this check is not needed 
    // since we always call isWriteConsistent first
    // it is here for sanity / future proofing in the case that we dont call isWriteConsistent    
    if (isLocked(sl)) {
        if (getTid(sl) == tid) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        assert(isUnlocked(sl));
        uint64_t newSL = (LOCKED) | (tid << TID_SHIFT) | (getTS(sl));         
        return mutex->compare_exchange_strong(sl, newSL);
    }
}
static const uint64_t EMPTY_SNAPSHOT_TIMESTAMP = 0;
static const int TBD_BIT_SHIFT = 1;
static const uint64_t TBD_SNAPSHOT_MASK = 0x1ULL;


inline __attribute__((always_inline)) static uint64_t makeRegularSnapshotTimestamp(uint64_t timestamp) {
    return (timestamp << TBD_BIT_SHIFT);
}

inline __attribute__((always_inline)) static uint64_t makeTBDSnapshotTimestamp(uint64_t timestamp) {
    return ((timestamp << TBD_BIT_SHIFT) | 1);
}


inline __attribute__((always_inline)) static uint64_t getUnmarkedTimestamp(uint64_t TBDmarkedTimestamp) {
    return (TBDmarkedTimestamp >> TBD_BIT_SHIFT);
}



struct DebugVersionedNode {
    uint64_t markedTimestamp;    
    uint64_t unmarkedTimestamp;    
    uint64_t data;    
};

struct VersionedNode {
    std::atomic<uint64_t> ts;    
    uint64_t data;    
};

struct VListTableNode {
    std::atomic<uint64_t*> addr;    
    std::atomic<uint64_t> headIdx;    
    uint64_t lastWriterTid;
    VListTableNode* next;
    VersionedNode list[VERSION_LIST_SIZE];        
};

struct LockTableNode {
    std::atomic<uint64_t> mutex;
    MVCC_TM::BloomFilter64Volatile bloom;
    // MVCC_TM::BloomFilter64 bloom;
};


struct VListPool {
    VListTableNode vtnPool[PER_THREAD_VLIST_POOL_INIT_CAPACITY];
    uint64_t count;
    VListPool* next;
};


struct Globals {
    PAD;
    LockTableNode gHashLock[NUM_LOCKS];
    PAD;
    std::atomic<uint64_t> gClock {1};    
    PAD;
    std::atomic<uint64_t> gSnapshotTs {1};
    PAD;
    // VListTableNode gVlistTable[NUM_LOCKS];
    VListTableNode *gVlistTable;
    PAD;
};

extern Globals globals;



// Random number generator used by the backoff scheme
inline __attribute__((always_inline)) static uint64_t marsagliaXORV(uint64_t x) {
    if (x == 0) x = 1;
    x ^= x << 6;
    x ^= x >> 21;
    x ^= x << 7;
    return x;
}


//
// Thread Registry stuff
//
extern void thread_registry_deregister_thread(const int tid);

// An helper class to do the checkin and checkout of the thread registry
struct ThreadCheckInCheckOut {
    static const int NOT_ASSIGNED = -1;
    int tid { NOT_ASSIGNED };
    ~ThreadCheckInCheckOut() {
        if (tid == NOT_ASSIGNED) return;
        thread_registry_deregister_thread(tid);
    }
};

extern thread_local ThreadCheckInCheckOut tl_tcico;

class ThreadRegistry;
extern ThreadRegistry gThreadRegistry;

/*
 * <h1> Registry for threads </h1>
 *
 * This is singleton type class that allows assignement of a unique id to each thread.
 * The first time a thread calls ThreadRegistry::getTID() it will allocate a free slot in 'usedTID[]'.
 * This tid wil be saved in a thread-local variable of the type ThreadCheckInCheckOut which
 * upon destruction of the thread will call the destructor of ThreadCheckInCheckOut and free the
 * corresponding slot to be used by a later thread.
 */
class ThreadRegistry {
private:
    static const bool kThreadPining = false;
    alignas(128) std::atomic<bool>      usedTID[REGISTRY_MAX_THREADS];   // Which TIDs are in use by threads
    alignas(128) std::atomic<int>       maxTid {-1};                     // Highest TID (+1) in use by threads

public:
    ThreadRegistry() {
        for (int it = 0; it < REGISTRY_MAX_THREADS; it++) {
            usedTID[it].store(false, std::memory_order_relaxed);
        }
    }

    // Progress condition: wait-free bounded (by the number of threads)
    int register_thread_new(void) {
        for (int tid = 0; tid < REGISTRY_MAX_THREADS; tid++) {
            if (usedTID[tid].load(std::memory_order_acquire)) continue;
            bool unused = false;
            if (!usedTID[tid].compare_exchange_strong(unused, true)) continue;
            // Increase the current maximum to cover our thread id
            int curMax = maxTid.load();
            while (curMax <= tid) {
                maxTid.compare_exchange_strong(curMax, tid+1);
                curMax = maxTid.load();
            }
            tl_tcico.tid = tid;
            // If thread pinning enabled, set it here
            if (kThreadPining) pinThread(tid);
            return tid;
        }
        printf("ERROR: Too many threads, registry can only hold %d threads\n", REGISTRY_MAX_THREADS);
        assert(false);
        return -1;
    }

    // Progress condition: wait-free population oblivious
    inline void deregister_thread(const int tid) {
        usedTID[tid].store(false, std::memory_order_release);
    }

    // Progress condition: wait-free population oblivious
    static inline uint64_t getMaxThreads(void) {
        return gThreadRegistry.maxTid.load(std::memory_order_acquire);
    }

    // Progress condition: wait-free bounded (by the number of threads)
    static inline int getTID(void) {
        int tid = tl_tcico.tid;
        if (tid != ThreadCheckInCheckOut::NOT_ASSIGNED) return tid;
        return gThreadRegistry.register_thread_new();
    }

    // Handle pinning
    inline void pinThread(const int tid) {
        cpu_set_t set;
        // tid 0 is usually 'main' so we pin it to first CPU too
        if (tid <= 20) {  // Our server (castor-1) has 10 cores => 20 HW threads and two CPUs
            CPU_ZERO(&set);
            sched_setaffinity(getpid(), sizeof(set), &set);
        }
    }
};

// Needed by our benchmarks and to prevent destructor from being called automatically on abort/retry
struct tmbase {
    ~tmbase() { }
};


struct ReadSet {
    struct ReadSetEntry {
        std::atomic<uint64_t>* mutex;        
    };

    // We pre-allocate a read-set with this many entries and if more are needed,
    // we grow them dynamically during the transaction and then delete them when
    static const int64_t MAX_ENTRIES = 16*1024;

    ReadSetEntry  entries[MAX_ENTRIES];
    uint64_t      size {0};          // Number of loads in the readSet for the current transaction
    ReadSet*      next {nullptr};

    inline void reset() {
        if (next != nullptr) {
            next->reset();   // Recursive call on the linked list of logs
            delete next;
            next = nullptr;
        }
        size = 0;
    }

    inline bool validate(uint64_t rClock, uint64_t tid) {
        for (uint64_t i = 0; i < size; i++) {
            std::atomic<uint64_t>* mutex = entries[i].mutex;
            uint64_t sl = mutex->load(std::memory_order_acquire);
            if (!isReadConsistent(rClock, tid, sl)) {
                return false;            
            }
        }
        if (next != nullptr) return next->validate(rClock, tid);
        return true;
    }

    inline void add(std::atomic<uint64_t>* mutex) {
        if (size != MAX_ENTRIES) {
            entries[size].mutex = mutex;
            size++;
        } else {
            // The current node is full, therefore, search a vacant node or create a new one
            if (next == nullptr) next = new ReadSet();
            next->add(mutex);
        }
    }

    bool contains(std::atomic<uint64_t>* mutex) {
        for (uint64_t i = 0; i < size; i++) {
            std::atomic<uint64_t>* m = entries[i].mutex;
            if (m == mutex) {
                return true;            
            }
        }
        if (next != nullptr) return next->contains(mutex);
        return true;
    }
};

// Volatile log (write-set)
struct AppendLog {
    // We pre-allocate a write-set with this many entries and if more are needed,
    // we grow them dynamically during the transaction and then delete them when
    // static const int64_t MAX_ENTRIES = 8*1024;
    static const int64_t MAX_ENTRIES = 7*1024;

    struct LogEntry {
        uint64_t* addr;
        uint64_t  oldValue;
    };

    int64_t       size {0};
    LogEntry      entries[MAX_ENTRIES];
    AppendLog*    next {nullptr};

    inline void reset() {
        if (next != nullptr) {
            next->reset();   // Recursive call on the linked list of logs
            delete next;
            next = nullptr;
        }
        size = 0;
    }

    // Adds VR ranges to the log
    inline void add(uint64_t* addr, uint64_t oldValue) {
        if (size != MAX_ENTRIES) {
            entries[size].addr = addr;
            entries[size].oldValue = oldValue;
            size++;
        } else {
            // The current node is full, therefore, search a vacant node or create a new one
            if (next == nullptr) {
                next = new AppendLog();
            }
            next->add(addr, oldValue); // recursive call to add()
        }
    }

    // Rollback modifications on VR: called from abortTx() to revert changes.
    void rollback(uint64_t tid) {
        if (size == 0) return;
        if (next != nullptr) {            
            next->rollback(tid); // Recursive call to rollbackVR()
        }
        for (int64_t i = size-1; i >= 0; i--) {
            *(entries[i].addr) = entries[i].oldValue;                
        }        
    }

    // Unlock all the locks acquired by this thread
    inline void unlock(uint64_t nextClock, uint64_t tid) {
        for (uint64_t i = 0; i < size; i++) {
            uint64_t* addr = entries[i].addr;
            std::atomic<uint64_t>* mutex = &globals.gHashLock[hidx((size_t)addr)].mutex;
            uint64_t sl = mutex->load(std::memory_order_relaxed);            
            if (isLocked(sl) && getTid(sl) == tid) {
                uint64_t newSL = (tid << TID_SHIFT) | (nextClock); 
                mutex->store(newSL, std::memory_order_release);                
            }
        }
        if (next != nullptr) {
            next->unlock(nextClock, tid);  // Recursive cal to unlock()
        }
    }
};


struct AppendLog2 {
    // We pre-allocate a write-set with this many entries and if more are needed,
    // we grow them dynamically during the transaction and then delete them when
    // static const int64_t MAX_ENTRIES = 8*1024;
    // static const int64_t MAX_ENTRIES = 2*1024;
    static const int64_t MAX_ENTRIES = 512;

    struct LogEntry {
        uint64_t* addr;      
        uint64_t oldValue; 
        VListTableNode* vtn; 
        uint64_t vtnListHead;
    };


    int64_t             size {0};
    LogEntry    entries[MAX_ENTRIES];
    AppendLog2*          next {nullptr};

    inline void reset() {
        if (next != nullptr) {
            next->reset();   // Recursive call on the linked list of logs
            delete next;
            next = nullptr;
        }
        size = 0;
    }

    // Adds VR ranges to the log
    inline void add(uint64_t* addr, uint64_t oldValue, VListTableNode* vtn, uint64_t vtnListHead) {
        if (size != MAX_ENTRIES) {
            entries[size].addr = addr;
            entries[size].oldValue = oldValue;            
            entries[size].vtn = vtn;   
            entries[size].vtnListHead = vtnListHead;         
            size++;            
        } else {
            // The current node is full, therefore, search a vacant node or create a new one
            if (next == nullptr) {
                next = new AppendLog2();
            }
            next->add(addr, oldValue, vtn, vtnListHead); // recursive call to add()
        }
    }

    // remove the added TBD from version list called from abortTx() to revert changes.
    inline void rollback(uint64_t tid) {
        if (size == 0) return;        
        if (next != nullptr) {            
            next->rollback(tid); // Recursive call to rollbackVR()
        }
        for (int64_t i = size-1; i >= 0; i--) {
            VListTableNode* vtn = entries[i].vtn;
            VersionedNode* vNode = &(vtn->list[entries[i].vtnListHead % VERSION_LIST_SIZE]);                        
            vNode->ts.store(EMPTY_SNAPSHOT_TIMESTAMP, std::memory_order_relaxed);            
            vNode->data = tid;
            *entries[i].addr = entries[i].oldValue;                                       
        }
    }

    __attribute__((noinline)) void removeTBDs(uint64_t tid, uint64_t firstReadSnapshotTimestamp, uint64_t lastReadSnapshotTimestamp) {                
        for (uint64_t i = 0; i < size; i++) {    
            // This addr cannot become unversioned while we hold the lock on it             
            VListTableNode* vtn = entries[i].vtn;
            uint64_t h = entries[i].vtnListHead;
            VersionedNode* vNode = &(vtn->list[h % VERSION_LIST_SIZE]);
       
            if (firstReadSnapshotTimestamp != lastReadSnapshotTimestamp) { 
                //timestamp change observed cant avoid indirection
                vNode->ts.store(makeRegularSnapshotTimestamp(lastReadSnapshotTimestamp), std::memory_order_release);
                vtn->headIdx.store(h+1, std::memory_order_release);
            }
            else {
                //we did NOT observe any changes in timestamp 
                // we might be able to avoid indirection if the previous node 
                // has the same timestamp as our node
                uint64_t prevIdx = h - 1;

                VersionedNode* prevNode = &vtn->list[prevIdx % VERSION_LIST_SIZE];
                if (getUnmarkedTimestamp(prevNode->ts.load(std::memory_order_relaxed)) == firstReadSnapshotTimestamp) {
                    prevNode->data = vNode->data;                    
                    vNode->ts.store(EMPTY_SNAPSHOT_TIMESTAMP, std::memory_order_release);
                }
                else {
                    vNode->ts.store(makeRegularSnapshotTimestamp(lastReadSnapshotTimestamp), std::memory_order_release);
                    vtn->headIdx.store(h+1, std::memory_order_release);
                }
            }
        }
        if (next != nullptr) {
            next->removeTBDs(tid, firstReadSnapshotTimestamp, lastReadSnapshotTimestamp);  // Recursive cal to unlock()
        }
    }

    // Unlock all the locks acquired by this thread
    inline void unlock(uint64_t nextClock, uint64_t tid) {
        for (uint64_t i = 0; i < size; i++) {
            void* addr = entries[i].addr;
            std::atomic<uint64_t>* mutex = &globals.gHashLock[hidx((size_t)addr)].mutex;
            uint64_t sl = mutex->load(std::memory_order_relaxed);            
            if (isLocked(sl) && getTid(sl) == tid) {
                // mutex->store((nextClock & TS_MASK), std::memory_order_release);
                uint64_t newSL = (tid << TID_SHIFT) | (nextClock); 
                mutex->store(newSL, std::memory_order_release);                
            }
        }
        if (next != nullptr) {
            next->unlock(nextClock, tid);  // Recursive cal to unlock()
        }
    }
};


// Thread-local data
struct OpData {    
    uint64_t     attempt {0};
    uint64_t     tid;
    uint64_t     rClock {0};     
    uint64_t     snapshotTs {0};
    uint64_t     snapshotTsTBD {0};
    int          tx_type {TX_IS_NONE};
    ReadSet      readSet;
    ReadSet      readSet2;
    AppendLog    writeSet;
    AppendLog2   snapshotWriteSet;
    uint64_t     myrand;    
    VListPool*   myVlistPool;    
    uint64_t     numAborts {0};
    uint64_t     numCommits {0};
    uint64_t     numSnapAborts {0};
    uint64_t     numSnapCommits {0};
    std::jmp_buf env;
    uint64_t     wasWriter {0};
    uint64_t     dbgCounter {0};
    uint64_t     padding[7];    
    // uint64_t     padding[8];    
};

[[noreturn]] extern void abortTx(OpData* myd);

// This is used by addToLog() to know which OpData instance to use for the current transaction
alignas(128) extern thread_local OpData* tl_opdata;

// T is typically a pointer to a node, but it can be integers or other stuff, as long as it fits in 64 bits
template<typename T> 
struct tx_field {
    static_assert(sizeof(T) <= sizeof(uint64_t));
    
    T data;    
    uint8_t pad[sizeof(uint64_t)-sizeof(T)];

    tx_field() { 
    }

    tx_field(T initVal) { store(initVal); }

    // Casting operator
    operator T() { return load(); }
    // Casting to const
    operator T() const { return load(); }

    // Prefix increment operator: ++x
    void operator++ () { store(load()+1); }
    // Prefix decrement operator: --x
    void operator-- () { store(load()-1); }
    void operator++ (int) { store(load()+1); }
    void operator-- (int) { store(load()-1); }
    tx_field<T>& operator+= (const T& rhs) { store(load() + rhs); return *this; }
    tx_field<T>& operator-= (const T& rhs) { store(load() - rhs); return *this; }

    // Equals operator
    template <typename Y, typename = typename std::enable_if<std::is_convertible<Y, T>::value>::type>
    bool operator == (const tx_field<Y> &rhs) { return load() == rhs; }
    // Difference operator: first downcast to T and then compare
    template <typename Y, typename = typename std::enable_if<std::is_convertible<Y, T>::value>::type>
    bool operator != (const tx_field<Y> &rhs) { return load() != rhs; }
    // Relational operators
    template <typename Y, typename = typename std::enable_if<std::is_convertible<Y, T>::value>::type>
    bool operator < (const tx_field<Y> &rhs) { return load() < rhs; }
    template <typename Y, typename = typename std::enable_if<std::is_convertible<Y, T>::value>::type>
    bool operator > (const tx_field<Y> &rhs) { return load() > rhs; }
    template <typename Y, typename = typename std::enable_if<std::is_convertible<Y, T>::value>::type>
    bool operator <= (const tx_field<Y> &rhs) { return load() <= rhs; }
    template <typename Y, typename = typename std::enable_if<std::is_convertible<Y, T>::value>::type>
    bool operator >= (const tx_field<Y> &rhs) { return load() >= rhs; }

    // Operator arrow ->
    T operator->() { return load(); }

    // Copy constructor
    tx_field<T>(const tx_field<T>& other) { store(other.load()); }

    // Assignment operator from a tx_field<T>
    tx_field<T>& operator=(const tx_field<T>& other) {
        store(other.load());
        return *this;
    }

    // Assignment operator from a value
    tx_field<T>& operator=(T value) {
        store(value);
        return *this;
    }

    // Operator &
    T* operator&() {
        return (T*)this;
    }    

    // returns true and stores the data in _val if we versioned this address 
    // returns false if we didnt version the address and need to retry
    // aborts the txn if we version the address but our initial read was not consistent
    __attribute__((noinline)) bool tryVersionAddr(T& _val, OpData* myd, uint64_t* addr, uint64_t tableIndex, std::atomic<uint64_t>* mutex, LockTableNode* ltn) const {
        //acquire lock
        uint64_t sl = mutex->load(std::memory_order_acquire);
        while (!tryWriteLock(myd->tid, mutex, sl)) {        
            sl = mutex->load(std::memory_order_acquire);
        }
        
        bool wasReadConsistent = true;
        if (getTS(sl) >= myd->rClock) {
            wasReadConsistent = false;
        }

        //look for this addr in the vlist table (or the first empty vtn)
        // we needto do this again since we didnt hold the lock
        // during our first traversal 
        // we do this before the recheck of the bloom filter since
        // even if the bloom filter contains the address this could be  
        // a false positive, and we need to confirm whether or not the address
        // has a vtn
        // The address could have a vtn already if someone else versioned it
        // after our first traversal
        VListTableNode* prevVtn = nullptr;
        VListTableNode* vtn = &globals.gVlistTable[tableIndex];
        uint64_t* nodeAddr;
        bool found = false;
        while (vtn) {
            nodeAddr = vtn->addr.load(std::memory_order_relaxed);
            if (nodeAddr == addr) {
                found = true;                
                break;
            }
            // if (nodeAddr == nullptr) {
            //     vtn->addr.store(addr, std::memory_order_release);
            //     break;
            // }
            prevVtn = vtn;
            vtn = vtn->next;
        }

        //check to ensure no one else versioned this address after we read the bloom filter
        if (!ltn->bloom.contains(addr)) {
            ltn->bloom.insertFreshUnsafe(addr);                
        }

        if (found) {
            //if we are a update txn we might have already claimed the lock in a write
            // we wont unlock if that was the case
            if (isUnlocked(sl)) {                
                //unlock to the same clock that we acquired on 
                uint64_t newSL = (myd->tid << TID_SHIFT) | (getTS(sl)); 
                mutex->store(newSL, std::memory_order_release);  
            } 
            else {
                myd->dbgCounter++;
            }
            return false;            
        }
        else if (!vtn) {
            if (myd->myVlistPool->count >= PER_THREAD_VLIST_POOL_INIT_CAPACITY) {
                VListPool* newVlp = (VListPool*)malloc(sizeof(VListPool));
                newVlp->next = myd->myVlistPool;
                myd->myVlistPool = newVlp;
            }                    
            vtn = &myd->myVlistPool->vtnPool[myd->myVlistPool->count++];
            vtn->addr.store(addr, std::memory_order_relaxed);
            prevVtn->next = vtn; 
        }

        //reread global snapshot timestamp to get latest
        uint64_t currentTs = globals.gSnapshotTs.load();            
        
        //write current version marked TBD            
        vtn->list[0].ts.store(makeTBDSnapshotTimestamp(currentTs), std::memory_order_release);

        T val = data;
        //write current value
        vtn->list[0].data = (uint64_t)val;
        vtn->lastWriterTid = myd->tid;

        //remove tbd
        vtn->list[0].ts.store(makeRegularSnapshotTimestamp(currentTs), std::memory_order_release);
                                
        //if we are a update txn we might have already claimed the lock in a write
        // we wont unlock if that was the case
        if (isUnlocked(sl)) {                
            //unlock to the same clock that we acquired on 
            uint64_t newSL = (myd->tid << TID_SHIFT) | (getTS(sl)); 
            mutex->store(newSL, std::memory_order_release);  
        }
        else {
            myd->dbgCounter++;
        }

        if (wasReadConsistent) {            
            _val = val;
            return true;
        }
        else {
            STATS_COUNTER_ADD(myd->tid, aborts_snapshot_tryVersion_read_inconsistent, 1);
            abortTx(myd);                    
        }
    }

    __attribute__((noinline)) T snapshotLoad(OpData* myd, uint64_t* addr, uint64_t tableIndex, LockTableNode* ltn) const {
        SNAPSHOT_RETRY:

        std::atomic<uint64_t>* mutex = &ltn->mutex;
        uint64_t sl = mutex->load(std::memory_order_acquire);

        if (getTid(sl) == myd->tid) {
            return (T)(*addr);
        }
        
        VListTableNode* vtn = &globals.gVlistTable[tableIndex];
        uint64_t* nodeAddr;
        bool found = false;
        bool wasReadConsistent;
        T val;

        if (ltn->bloom.contains(addr)) {
            while (vtn) {
                nodeAddr = vtn->addr.load(std::memory_order_relaxed);
                if (nodeAddr == addr) {
                    found = true;                
                    break;
                }                                   
                vtn = vtn->next;
            }
        }

        if (!found) {        
            // version this address            
            if (tryVersionAddr(val, myd, addr, tableIndex, mutex, ltn)) {
                return val;
            }
            else {
                goto SNAPSHOT_RETRY;
            }            
        }   

        myd->readSet2.add(mutex);

        uint64_t vNodeIdx = vtn->headIdx.load(std::memory_order_acquire);
        VersionedNode* vNode = &vtn->list[vNodeIdx % VERSION_LIST_SIZE];        
        uint64_t markedTs1 = vNode->ts.load(std::memory_order_acquire);
        found = false;    
        if (getTid(sl) != myd->tid) {
            while ((markedTs1 & TBD_SNAPSHOT_MASK) && (markedTs1 <= myd->snapshotTsTBD)) {
                // SOFTWARE_BARRIER;
                markedTs1 = vNode->ts.load(std::memory_order_acquire);                
                found = true;
            }            
        }
        else {
            return (T)(vNode->data);
        }

        uint64_t h2 = vtn->headIdx.load(std::memory_order_acquire);
        if (vNodeIdx != h2) {
            goto SNAPSHOT_RETRY;
        }

        uint64_t markedTs2;
        if (!found) {
            uint64_t prevIdx = (vNodeIdx - 1);
            VersionedNode* vNode2 = &vtn->list[prevIdx % VERSION_LIST_SIZE];
            markedTs2 = vNode2->ts.load(std::memory_order_acquire);

            if (markedTs2 & TBD_SNAPSHOT_MASK) {                    
                goto SNAPSHOT_RETRY;
            }
            else {
                if (markedTs1 < markedTs2) { //slight concern that the check for ts2 being tbd could be reordered after this
                    vNodeIdx = prevIdx;
                    vNode = vNode2;
                    markedTs1 = markedTs2;
                }
            }
        }
        
        uint64_t probeCount = 0;           
        uint64_t nonEmptyProbeCount = 0;   
        while ((getUnmarkedTimestamp(markedTs1) > myd->snapshotTs) || markedTs1 == EMPTY_SNAPSHOT_TIMESTAMP) {
            if (probeCount >= VERSION_LIST_SIZE) {
                STATS_COUNTER_ADD(myd->tid, aborts_snapshot_vlist_failed_find_ts, 1);
                abortTx(myd);
            }
            if (markedTs1 != EMPTY_SNAPSHOT_TIMESTAMP) {
                nonEmptyProbeCount++;
            }            
            vNodeIdx = (vNodeIdx - 1);
            vNode = &(vtn->list[vNodeIdx % VERSION_LIST_SIZE]);
            markedTs1 = vNode->ts.load(std::memory_order_acquire);
            probeCount++;                
        }         

        //we dont need to check if ts1 is TBD here because we 
        // confirmed earlier that we started our traversal at a non-TBD
        // timestamp so any new TBDs will change the version to one that is larger than our snapshotTS
        // which we will check after reading the value        
        // if (ts1 & TBD_SNAPSHOT_MASK) {
        //     abortTx(myd);
        // }

        val = (T)(vNode->data);

        // if we make it here then we started at h or h-1
        // both of which were not marked TBD
        // the only way that this node can change is if we fully circle around the array
        // and this nodes timestamp is updated        
        markedTs2 = vNode->ts.load(std::memory_order_acquire);
        nodeAddr = vtn->addr.load(std::memory_order_acquire);
        if (markedTs2 != markedTs1 || nodeAddr != addr) {                    
            goto SNAPSHOT_RETRY;
        }
        return val; 
    }

    bool tryVersionedStore(OpData* myd, uint64_t* addr, T newVal, VListTableNode* vtn, LockTableNode* ltn) {
        if (tryUnversion(myd, vtn, addr, ltn, UNVERSION_RANDOM_ROLL_THRESHOLD_WRITER)) {
            return false;
        }
        
        //check if the addr has a vlist - if it does not then we hit a false positive in the bloom filter
        bool found = false;
        uint64_t* nodeAddr;
        while (vtn) {
            nodeAddr = vtn->addr.load(std::memory_order_relaxed);
            if (nodeAddr == addr) {
                found = true;
                break;
            }
            vtn = vtn->next;
        }
                
        if (!found) {
            return false;
        }        

        uint64_t h = vtn->headIdx.load(std::memory_order_relaxed);
        VersionedNode* vNode = &vtn->list[h % VERSION_LIST_SIZE];
        if (!(vNode->ts.load(std::memory_order_relaxed) & TBD_SNAPSHOT_MASK)) {            
            //this is the first time we are writting to this address
            vNode->ts.store(myd->snapshotTsTBD, std::memory_order_release);
            // SOFTWARE_BARRIER;
            vNode->data = (uint64_t)newVal;
            myd->snapshotWriteSet.add(addr, *addr, vtn, h); 
            vtn->lastWriterTid = myd->tid;
        }     
        else {            
            vNode->data = (uint64_t)newVal;            
        }

        return true;
    }

    bool tryUnversion(OpData* myd, VListTableNode* vtn, uint64_t* addr, LockTableNode* ltn, uint64_t unversionThreshold, std::atomic<uint64_t>* mutex = nullptr) {
        //roll the dice to see if we should unversion            
        //if rand < threshold: reset bloom filter, null the version lists
        if (vtn->lastWriterTid != myd->tid) {
            vtn->lastWriterTid = myd->tid;
            myd->myrand = marsagliaXORV(myd->myrand);    
            if (myd->myrand < unversionThreshold) {
                if (mutex) {
                    uint64_t sl = mutex->load(std::memory_order_acquire);
                    if (!tryWriteLock(myd->tid, mutex, sl)) {
                        return false;
                    }
                }

                ltn->bloom.init();
#ifdef ENABLE_VTN_ZEROING
                while (vtn) {
                    // vtn->addr.store(IN_PROGRESS_UNVERSIONING_ADDR, std::memory_order_relaxed);
                    vtn->addr.store(IN_PROGRESS_UNVERSIONING_ADDR, std::memory_order_release);
                    vtn->headIdx.store(0, std::memory_order_relaxed);
                    for (int i = 0; i < VERSION_LIST_SIZE; i++) {
                        vtn->list[i].ts.store(EMPTY_SNAPSHOT_TIMESTAMP, std::memory_order_relaxed);
                    }
                    vtn->addr.store(UNVERSIONED_ADDR, std::memory_order_release);                     
                    vtn = vtn->next;
                }
#endif                
                return true;
            } 
        }

        return false;
    }

    __attribute__((noinline)) void snapshotStore(OpData* myd, uint64_t* addr, T newVal, uint64_t tableIndex, LockTableNode* ltn, std::atomic<uint64_t>* mutex, bool noUnversion = false) {
        //  - claim lock regardless of version        
        //check if versioned
        //if versioned         
        //  - write TBD into version list
        //  - at commit time swap TBD to current snapshot TS
        //if unversioned        
        //  - write to data directly
        uint64_t sl = mutex->load(std::memory_order_acquire);

        while (!tryWriteLock(myd->tid, mutex, sl)) {            
            sl = mutex->load(std::memory_order_acquire);
        }

        VListTableNode* vtn = &globals.gVlistTable[tableIndex];        
        //check if the addr has a vlist - if it does not then we hit a false positive in the bloom filter
        bool found = false;
        uint64_t* nodeAddr;
        while (vtn) {
            nodeAddr = vtn->addr.load(std::memory_order_relaxed);
            if (nodeAddr == addr) {
                found = true;
                break;
            }
            vtn = vtn->next;
        }
        
        if (!found) {                        
            myd->writeSet.add(addr, *addr);
            return;
        }        

        uint64_t h = vtn->headIdx.load(std::memory_order_relaxed);
        VersionedNode* vNode = &vtn->list[h % VERSION_LIST_SIZE];
        if (!(vNode->ts.load(std::memory_order_relaxed) & TBD_SNAPSHOT_MASK)) {            
            //this is the first time we are writting to this address
            vNode->ts.store(myd->snapshotTsTBD, std::memory_order_release);
            // SOFTWARE_BARRIER;
            vNode->data = (uint64_t)newVal;
            myd->snapshotWriteSet.add(addr, *addr, vtn, h); 
            vtn->lastWriterTid = myd->tid;
        }     
        else {            
            vNode->data = (uint64_t)newVal;            
        }                
    }

    void txStore(OpData* myd, uint64_t* addr, T newVal, uint64_t tableIndex, LockTableNode* ltn, std::atomic<uint64_t>* mutex) {
        uint64_t sl = mutex->load(std::memory_order_acquire);
        if (!isWriteConsistent(myd->rClock, myd->tid, sl)) {
            STATS_COUNTER_ADD(myd->tid, aborts_tl2_store_write_inconsistent, 1);
            abortTx(myd);
        }                      
        if (!tryWriteLock(myd->tid, mutex, sl)) {
            STATS_COUNTER_ADD(myd->tid, aborts_tl2_store_failed_lock_acquire, 1);
            abortTx(myd);
        }        

#ifndef DISABLE_MVCC
        if (__glibc_unlikely(ltn->bloom.contains(addr))) {   
            VListTableNode* vtn = &globals.gVlistTable[tableIndex];
            if (!tryVersionedStore(myd, addr, newVal, vtn, ltn)) {
                myd->writeSet.add(addr, (uint64_t)data); 
            }            
        }
        else {            
            myd->writeSet.add(addr, (uint64_t)data); // save the previous data    
        }                
#else        
        myd->writeSet.add(addr, (uint64_t)data); // save the previous data    
#endif
    }

    // Store interposing: acquire the lock and write in 'main'
    inline void store(T newVal) {
        OpData* const myd = tl_opdata;
        uint64_t* addr = (uint64_t*)&data;                   

        uint64_t tableIndex = hidx((size_t)addr);
        LockTableNode* ltn = &globals.gHashLock[tableIndex];
        std::atomic<uint64_t>* mutex = &ltn->mutex;

        if (myd->tx_type == TX_IS_SNAPSHOT) {
#ifdef ENABLE_WRITE_SNAPSHOT
            snapshotStore(myd, addr, newVal, tableIndex, ltn, mutex);
#else 
            printf("ERROR: writer on snapshot path\n");
            exit(-1);
#endif
        }
        else {
            txStore(myd, addr, newVal, tableIndex, ltn, mutex);
        }

        data = newVal;
    }

    // This is similar to an undo-log load interposing: do a single post-check
    inline T load() {        
        OpData* const myd = tl_opdata;
        uint64_t* addr = (uint64_t*)(&data);        
        uint64_t tableIndex = hidx((size_t)addr);
        LockTableNode* ltn = &globals.gHashLock[tableIndex];   
        T val = data;

#ifndef DISABLE_MVCC        
        if (myd->tx_type == TX_IS_SNAPSHOT) {
            return snapshotLoad(myd, addr, tableIndex, ltn);                                    
        }           
#endif  
        std::atomic<uint64_t>* mutex = &ltn->mutex;
        uint64_t sl = mutex->load(std::memory_order_acquire);
        if (!isReadConsistent(myd->rClock, myd->tid, sl)) {
            STATS_COUNTER_ADD(myd->tid, aborts_tl2_load_read_inconsistent, 1);
            abortTx(myd);
        }        
        if (myd->tx_type == TX_IS_UPDATE) {
            myd->readSet.add(mutex);
        }
        VListTableNode* vtn = &globals.gVlistTable[tableIndex];
        tryUnversion(myd, vtn, addr, ltn, UNVERSION_RANDOM_ROLL_THRESHOLD_READER, mutex);
        return val;     
    }
};



class MVCC_DCOTL;
extern MVCC_DCOTL gDCOTL;

class MVCC_DCOTL {
private:
    alignas(128) OpData                   *opDesc;
    alignas(128) std::atomic<int>         debugLock {0};

public:

    TMAlloc                                tmAlloc {};

    struct tmbase : public ns_mvcc_tm::tmbase { };

    MVCC_DCOTL() {
        printf("sizeof(OpData)=%ld\n", sizeof(OpData));
        opDesc = new OpData[REGISTRY_MAX_THREADS];
        for (uint64_t it=0; it < REGISTRY_MAX_THREADS; it++) {
            opDesc[it].tid = it;
            opDesc[it].myrand = (it+1)*12345678901234567ULL;
            opDesc[it].myVlistPool = nullptr;
            opDesc[it].myVlistPool = (VListPool*)malloc(sizeof(VListPool));            
        }

        globals.gVlistTable = (VListTableNode*)malloc(sizeof(VListTableNode) * NUM_LOCKS);

#ifdef ENABLE_VTN_ZEROING
        for (uint64_t i = 0; i < NUM_LOCKS; i++) {
            globals.gVlistTable[i].addr = UNVERSIONED_ADDR;            
        }
#endif

        mapVolatileRegion();
    }

    ~MVCC_DCOTL() {
        uint64_t totalAborts = 0;
        uint64_t totalCommits = 0;
#ifdef TRACK_SNAPSHOT_METRICS        
        uint64_t totalSnapCommits = 0;
        uint64_t totalSnapAborts = 0;
#endif
        // uint64_t* tableHits = new uint64_t[NUM_LOCKS];
        for (int it=0; it < REGISTRY_MAX_THREADS; it++) {
            totalAborts += opDesc[it].numAborts;
            totalCommits += opDesc[it].numCommits;
#ifdef TRACK_SNAPSHOT_METRICS        
            totalSnapCommits += opDesc[it].numSnapCommits;
            totalSnapAborts += opDesc[it].numSnapAborts;
#endif    
        }
        printf("tx_num_aborts=%ld\n", totalAborts);        
        printf("tx_num_commits=%ld\n\n", totalCommits);        
#ifdef TRACK_SNAPSHOT_METRICS        
        printf("tx_num_snapshot_aborts=%ld\n", totalSnapAborts);
        printf("tx_num_snapshot_commits=%ld\n\n", totalSnapCommits);
#endif        

        delete[] opDesc;
    }

    // Maps the volatile memory region
    void mapVolatileRegion() {
        void* mapAddr = mmap(NULL, VOLATILE_MEMORY_REGION_SIZE, (PROT_READ | PROT_WRITE), MAP_SHARED | MAP_ANONYMOUS, -1, 0);
        
        if (mapAddr == MAP_FAILED) {
            perror("mmap error:\n ");
            printf("-------------\n");
            exit(-1);
        }

        this->tmAlloc.init(mapAddr, VOLATILE_MEMORY_REGION_SIZE);
    }

    inline void beginTx(OpData* myd, const int tid) {
        myd->rClock = globals.gClock.load(); 
        if (myd->tx_type == TX_IS_SNAPSHOT) {                        
            myd->snapshotTs = globals.gSnapshotTs.fetch_add(1);
            myd->snapshotTsTBD = makeTBDSnapshotTimestamp(myd->snapshotTs);                        
        }
        else if (myd->tx_type == TX_IS_UPDATE) {
            myd->snapshotTs = globals.gSnapshotTs.load();            
            myd->snapshotTsTBD = makeTBDSnapshotTimestamp(myd->snapshotTs);
        }

        // Clear the logs of the previous transaction
        myd->writeSet.reset();
        myd->snapshotWriteSet.reset();
        myd->readSet.reset();        
        myd->readSet2.reset();        
    }

    // This PTM does eager locking, which means that by now all locks have been acquired.
    inline bool endTx(OpData* myd, const int tid) {        
        // Check if this is a read-only transaction and if so, commit immediately
        if (myd->writeSet.size == 0 && myd->snapshotWriteSet.size == 0) {            
            myd->attempt = 0;
#ifdef TRACK_REGULAR_METRICS        
            myd->numCommits++;
#endif
#ifdef TRACK_SNAPSHOT_METRICS
            if (myd->tx_type == TX_IS_SNAPSHOT) {                      
                myd->numSnapCommits++;
            }
#endif
            this->tmAlloc.commit(myd->tid);
            return true;
        }

        // This fence is needed by undo log to prevent re-ordering with the last store
        // and the reading of the gClock.
        // std::atomic_thread_fence(std::memory_order_seq_cst);
        // Validate the read-set
        if (!myd->readSet.validate(myd->rClock, tid)) {
            abortTx(myd);
        }
    
        this->tmAlloc.commit(myd->tid);

        uint64_t commitTS = globals.gClock.load(); // timestamp allocation        
        uint64_t commitSnapshotTs = globals.gSnapshotTs.load();        
        myd->snapshotWriteSet.removeTBDs(myd->tid, myd->snapshotTs, commitSnapshotTs);

        myd->writeSet.unlock(commitTS, tid);
        myd->snapshotWriteSet.unlock(commitTS, tid);
#ifdef TRACK_REGULAR_METRICS        
        myd->numCommits++;
#endif
#ifdef TRACK_SNAPSHOT_METRICS
        if (myd->tx_type == TX_IS_SNAPSHOT) {                      
            myd->numSnapCommits++;
        }
#endif
        myd->attempt = 0;
        myd->wasWriter = 0;
        return true;
    }

    template<typename F> void transaction(F&& func, int txType=TX_IS_UPDATE) {
        if (tl_opdata != nullptr) {
            func();
            return ;
        }
        const int tid = ThreadRegistry::getTID();
        OpData* myd = &opDesc[tid];
        tl_opdata = myd;
        myd->tx_type = txType;
        setjmp(myd->env);
        myd->attempt++;
        backoff(myd, myd->attempt);
        beginTx(myd, tid);
        func();
        endTx(myd, tid);
        tl_opdata = nullptr;
    }

    // It's silly that these have to be static, but we need them for the (SPS) benchmarks due to templatization
    //template<typename R, typename F> static R updateTx(F&& func) { return gDCOTL.transaction<R>(func, TX_IS_UPDATE); }
    //template<typename R, typename F> static R readTx(F&& func) { return gDCOTL.transaction<R>(func, TX_IS_READ); }
    template<typename F> static void updateTx(F&& func) { gDCOTL.transaction(func, TX_IS_UPDATE); }
    template<typename F> static void readTx(F&& func) { gDCOTL.transaction(func, TX_IS_READ); }    

    // Backoff for a random amount of steps in the range [16, 16*attempt]. Inspired by TL2
    inline void backoff(OpData* myopd, uint64_t attempt) {
        if (attempt < 3) return;
        if (attempt == 10000) {
        // if (attempt == 100000) {
            // printf("Ooops, looks like we're stuck attempt=%ld\n", attempt);
            #ifdef USE_GSTATS
            int expected = 0;
            if (debugLock.compare_exchange_strong(expected, 1)) {
                printf("MAX ABORTS REACHED -- PRINTING GSTATS:\n");
                GSTATS_PRINT;
                printf("\n");
                // exit(-1);
            }             
            std::this_thread::yield();                     
            #endif    
            // exit(-1);
        }
        myopd->myrand = marsagliaXORV(myopd->myrand);
        uint64_t stall = (myopd->myrand & attempt) + 1;
        stall *= 16;
        std::atomic<uint64_t> iter {0};
        while (iter.load() < stall) iter.fetch_add(1);
        if (stall > 1000) std::this_thread::yield();
    }

    template<typename R,class F> inline static R readTx(F&& func) {
        gDCOTL.transaction([&]() {func();}, TX_IS_READ);
        return R{};
    }
    template<typename R,class F> inline static R updateTx(F&& func) {
        gDCOTL.transaction([&]() {func();}, TX_IS_UPDATE);
        return R{};
    }

    template <typename T, typename... Args> static T* tmNew(Args&&... args) {
        if (tl_opdata == nullptr) {
            printf("ERROR: Can not allocate outside a transaction\n");
            return nullptr;
        }
        void* ptr = gDCOTL.tmAlloc.tmMalloc(tl_opdata->tid, sizeof(T));

        // If we get nullptr then we've ran out of PM space
        assert(ptr != nullptr);
        // If there is an abort during the 'new placement', the transactions rolls
        // back and will automatically revert the changes in the allocator metadata.
        new (ptr) T(std::forward<Args>(args)...);  // new placement
        return (T*)ptr;
    }
    template<typename T> static void tmDelete(T* obj) {
        if (obj == nullptr) return;
        if (tl_opdata == nullptr) {
            printf("ERROR: Can not de-allocate outside a transaction\n");
            return;
        }
        obj->~T();
        gDCOTL.tmAlloc.tmFree(tl_opdata->tid, obj);
    }
    static void* tmMalloc(size_t size) {
        if (tl_opdata == nullptr) {
            printf("ERROR: Can not allocate outside a transaction\n");
            return nullptr;
        } 
        void* obj = gDCOTL.tmAlloc.tmMalloc(tl_opdata->tid, size);
        return obj;
    }
    static void tmFree(void* obj) {
#ifdef DISABLE_TM_FREE
        return;
#endif
        if (obj == nullptr) return;
        if (tl_opdata == nullptr) {
            printf("ERROR: Can not de-allocate outside a transaction\n");
            return;
        }
        gDCOTL.tmAlloc.tmFree(tl_opdata->tid, obj);               
    }
};


//
// Wrapper methods to the global TM instance. The user should use these:
//
// template<typename R, typename F> static R updateTx(F&& func) { return gDCOTL.transaction<R>(func, TX_IS_UPDATE); }
// template<typename R, typename F> static R readTx(F&& func) { return gDCOTL.transaction<R>(func, TX_IS_READ); }
// template<typename F> static void updateTx(F&& func) { gDCOTL.transaction(func, TX_IS_UPDATE); }
// template<typename F> static void readTx(F&& func) { gDCOTL.transaction(func, TX_IS_READ); }
// template<typename T, typename... Args> T* tmNew(Args&&... args) { return MVCC_DCOTL::tmNew<T>(args...); }
// template<typename T> void tmDelete(T* obj) { MVCC_DCOTL::tmDelete<T>(obj); }
// static void* tmMalloc(size_t size) { return MVCC_DCOTL::tmMalloc(size); }
// static void tmFree(void* obj) { MVCC_DCOTL::tmFree(obj); }


#ifdef MICROBENCH
//
// Place these in a .cpp if you include this header from multiple files (compilation units)
//
ThreadRegistry gThreadRegistry {};

Globals globals {};
// Globals* globals {};

MVCC_DCOTL gDCOTL {};

// Thread-local data of the current ongoing transaction
thread_local OpData* tl_opdata {nullptr};
// This is where every thread stores the tid it has been assigned when it calls getTID() for the first time.
// When the thread dies, the destructor of ThreadCheckInCheckOut will be called and de-register the thread.
thread_local ThreadCheckInCheckOut tl_tcico {};
// Helper function for thread de-registration
void thread_registry_deregister_thread(const int tid) {
    gThreadRegistry.deregister_thread(tid);
}

[[noreturn]] void abortTx(OpData* myd) {    

    if (myd->dbgCounter > 0) {
        printf("theres ur problem\n");
    }

    myd->writeSet.rollback(myd->tid);
    myd->snapshotWriteSet.rollback(myd->tid);
    uint64_t nextClock = globals.gClock.fetch_add(1);
    
#ifdef GSTATS_HANDLE_STATS
    STATS_LIST_APPEND(myd->tid, rdset_size_at_abort, myd->readSet.size);
    STATS_LIST_APPEND(myd->tid, wrset_size_at_abort, myd->writeSet.size);
    STATS_LIST_APPEND(myd->tid, snapshot_wrset_size_at_abort, myd->snapshotWriteSet.size);
#endif

    // Unlock with the next sequence
    myd->writeSet.unlock(nextClock, myd->tid);
    myd->snapshotWriteSet.unlock(nextClock, myd->tid);
#ifdef TRACK_REGULAR_METRICS
    myd->numAborts++;    
#endif
    
#ifdef ENABLE_WRITE_SNAPSHOT
    if (myd->attempt > RETRIES_BEFORE_SNAPSHOT) {
        if (myd->tx_type == TX_IS_UPDATE) {
            myd->wasWriter = 1;
        }
#else 
    if (myd->tx_type == TX_IS_READ && myd->attempt > RETRIES_BEFORE_SNAPSHOT) {
#endif
        myd->tx_type = TX_IS_SNAPSHOT;
    }
    
#ifdef TRACK_SNAPSHOT_METRICS
    if (myd->tx_type == TX_IS_SNAPSHOT) {
        myd->numSnapAborts++;
    }
#endif

    std::longjmp(myd->env, 1);
}
#endif // MICROBENCH

}

